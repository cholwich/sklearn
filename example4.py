import numpy as np
import numpy.random
from sklearn.linear_model import Perceptron
from sklearn.svm import SVC
import matplotlib.pyplot as plt

meanp = [1, 1]
covp = 0.5*np.identity(2)
Xp = np.random.multivariate_normal(meanp, covp, 50) 

meann = [3, 3]
covn = 0.7*np.identity(2)
Xn = np.random.multivariate_normal(meann, covn, 50)

X = np.r_[Xp, Xn]
t = np.r_[np.ones(50), np.zeros(50)]

plt.scatter(X[:, 0], X[:, 1], s=50, c=t, cmap=plt.cm.RdYlGn)

clf = Perceptron(n_iter=10000)
clf.fit(X, t)

x1, x2 = np.meshgrid(np.linspace(-1, 6, 100), 
                     np.linspace(-1, 6, 100))

y = clf.decision_function(np.c_[x1.ravel(), x2.ravel()])
y = y.reshape(x1.shape)

plt.contour(x1, x2, y, [0.5], linewidths=2, colors='b')

clf2 = SVC(kernel='poly', degree=4)
clf2.fit(X, t)

y = clf2.decision_function(np.c_[x1.ravel(), x2.ravel()])
y = y.reshape(x1.shape)

plt.contour(x1, x2, y, [0.5], linewidths=2, colors='m')

plt.show()
