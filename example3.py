import numpy as np
from sklearn.linear_model import Perceptron
import matplotlib.pyplot as plt

X = np.array([[0,0], [0,1], [1,0], [1,1]], dtype=float)
t = np.array([0,0,0,1])

clf = Perceptron(n_iter=10)
clf.fit(X, t)

plt.scatter(X[:, 0], X[:, 1], s=50, c=t, cmap=plt.cm.RdYlGn)

x1, x2 = np.meshgrid(np.linspace(-0.2, 1.2, 100), 
                     np.linspace(-0.2, 1.2, 100))

y = clf.decision_function(np.c_[x1.ravel(), x2.ravel()])
y = y.reshape(x1.shape)

plt.contour(x1, x2, y, [0.5], linewidths=2, colors='b')
plt.show()
