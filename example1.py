import numpy as np

u = np.array([2,3,5,7], dtype=float)
v = np.ones(4)

np.set_printoptions(precision=3)

print(u)
print(v)

x = u+v
print(x)

y = 4*v
print(y)

z = v
z[3] = 4
print(z); print(v)

a = np.copy(v)
a[3] = 5
print(a); print(v)

print(u[1:4])

b = np.array([0,3])
print(u[b])

print(np.dot(a, v))
print(np.sum(u))
print(np.amax(u))
print(np.argmax(u))
