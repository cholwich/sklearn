import numpy as np
import numpy.random
from sklearn.linear_model import Perceptron
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt

meanp = [1, 1]
covp = 0.5*np.identity(2)
Xtrp = np.random.multivariate_normal(meanp, covp, 50)
Xtsp = np.random.multivariate_normal(meanp, covp, 500)

meann = [3, 3]
covn = 0.7*np.identity(2)
Xtrn = np.random.multivariate_normal(meann, covn, 50)
Xtsn = np.random.multivariate_normal(meann, covn, 500)

Xtr = np.r_[Xtrp, Xtrn]
ttr = np.r_[np.ones(50), np.zeros(50)]

Xts = np.r_[Xtsp, Xtsn]
tts = np.r_[np.ones(500), np.zeros(500)]

clf1 = Perceptron(n_iter=10000)
clf1.fit(Xtr, ttr)
print("Perceptron result")
y = clf1.predict(Xtr)
acc = accuracy_score(ttr, y)
print("Training accuracy = " + "%.3f" % acc)
y = clf1.predict(Xts)
acc = accuracy_score(tts, y)
print("Test accuracy = " + "%.3f" % acc)

print("\nSVM with Polynomial Kernel Result")
clf2 = SVC(kernel='poly', degree=4)
clf2.fit(Xtr, ttr)
y = clf2.predict(Xtr)
acc = accuracy_score(ttr, y)
print("Training accuracy = " + "%.3f" % acc)
y = clf2.predict(Xts)
acc = accuracy_score(tts, y)
print("Test accuracy = " + "%.3f" % acc)
