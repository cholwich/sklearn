import numpy as np
import numpy.linalg

X = np.array([[1,2,3], [2,3,4], [3,4,5]], dtype=float)

np.set_printoptions(precision=3)

print(X)

print(X.shape)

print(np.linalg.inv(X))

Y = np.identity(3)

print(Y)

print(X*Y)

print(np.dot(X, Y))

Z = X.reshape((1,9))
print(Z)

W = X.ravel()
print(W)

V = np.r_[Z, Z]
print(V)

U = np.c_[Z, Z]
print(U)
